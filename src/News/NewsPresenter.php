<?php namespace Finnito\NewsModule\News;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class NewsPresenter extends EntryPresenter
{

    public function statusLabel($size = 'sm')
    {
        $color  = 'default';
        $status = $this->status();

        switch ($status) {
            case 'scheduled':
                $color = 'info';
                $conj = "for";
                break;

            case 'live':
                $color = 'success';
                $conj = "on";
                break;
        }

        return '<span class="tag tag-' . $size . ' tag-' . $color . '">' . trans(
            'anomaly.module.posts::field.status.option.' . $status
        ) . '</span>' . " $conj {$this->object->publishDate()}" ;
    }

    public function status()
    {
        if ($this->object->isPublished()) {
            return 'live';
        } else {
            return "scheduled";
        }

        return null;
    }
}
