<?php namespace Finnito\NewsModule\News;

use Finnito\NewsModule\News\Contract\NewsInterface;
use Anomaly\Streams\Platform\Model\News\NewsNewsEntryModel;

class NewsModel extends NewsNewsEntryModel implements NewsInterface
{
    public function isPublished()
    {
        return $this->date->format("Y-m-d G:i:s") <= date("Y-m-d G:i:s");
    }

    public function publishDate()
    {
        return $this->date->format("j M, Y \\a\\t g:ia");
    }
}
