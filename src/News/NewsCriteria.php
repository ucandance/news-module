<?php namespace Finnito\NewsModule\News;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

class NewsCriteria extends EntryCriteria
{
	public function currentNews() {
        return $this
            ->where("date", "<=", date("Y-m-d H:i:s"))
            ->orderBy("date", "DESC")
            ->first();
    }
}
