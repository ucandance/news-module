<?php namespace Finnito\NewsModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Finnito\NewsModule\News\Contract\NewsRepositoryInterface;

class NewsController extends PublicController
{

    public function index(NewsRepositoryInterface $news)
    {
        $this->template->set("meta_title", "News");
        $this->template->set("meta_description", "Latest news and announcements about the goings-on at UCanDance!");
        $this->breadcrumbs->add("News", $this->request->path());
        return $this->view->make(
            "finnito.module.news::index"
        );
    }
}
