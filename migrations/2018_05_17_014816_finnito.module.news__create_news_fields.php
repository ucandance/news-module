<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleNewsCreateNewsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        // 'name' => 'anomaly.field_type.text',
        // 'slug' => [
        //     'type' => 'anomaly.field_type.slug',
        //     'config' => [
        //         'slugify' => 'name',
        //         'type' => '_'
        //     ],
        // ],
        "content" => [
            "type" => "anomaly.field_type.wysiwyg",
            "config" => [
                "buttons" => [
                    "bold",
                    "italic",
                    "link",
                ],
                "plugins" => [

                ],
            ],
        ],
        "date" => [
            "type" => "anomaly.field_type.datetime",
            "config" => [
            ],
        ]
    ];

}
