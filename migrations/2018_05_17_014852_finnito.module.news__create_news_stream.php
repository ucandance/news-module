<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleNewsCreateNewsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'news',
        'title_column' => 'content',
        'translatable' => false,
        'trashable' => false,
        'searchable' => false,
        'sortable' => false,
        "versionable" => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'content' => [
            'translatable' => true,
            'required' => true,
        ],
        'date' => [
            'required' => true,
        ],
    ];

}
