<?php

return [
    'title'       => 'News',
    'name'        => 'News Module',
    'description' => 'Schedule & post small news announcements to the UCanDance website.'
];
