<?php

return [
	"content" => [
		"name" => "Content",
	],
	"date" => [
		"name" => "Date",
		"instructions" => "Set a date-time in the future to schedule the announcement.",
	],
];
